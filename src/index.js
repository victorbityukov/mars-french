//либа, которая пытается в лимматизацию
const Morphy = require('phpmorphy');
//стандартная либа для работы с путями
const path = require('path');
//либа, с помощью которой читаем файлы docx
const textract = require('textract');
//функции для обработки, которые вынесли отдельно
const { generateArr, contains, textProcessing } = require('./utils');

//создание экземпляра morphy, через который в дальнейшем будем работать
const morphy = new Morphy('en', {
  nojo: false,
  storage: Morphy.STORAGE_MEM,
  predict_by_suffix: true,
  predict_by_db: true,
  graminfo_as_text: true,
  use_ancodes_cache: false,
  resolve_ancodes: Morphy.RESOLVE_ANCODES_AS_TEXT,
});

//основная функция для вычисления всего и вся
const calcPercentFrench = (list, text) => {
  //проводим лемматизацию
  const textLemmatize = morphy.lemmatize(text);
  const frenchLemmatize = morphy.lemmatize(list);
  //количество французских заимствований
  let frenchAmount = 0;
  //количество слов всего
  let textAmount = 0;
  //объект в поляъ которого будем считать количество отдельных слов в тексте
  let frenchAmountTop = {};

  //проходим по тексту циклом
  for(let keyWordText in textLemmatize) {
    //прибавляем единицу к общему числу слов в тексте на каждой итерации цикла
    textAmount++;
    //приводим лематизированное слово к единому виду (массив)
    const wordsText = generateArr(keyWordText, textLemmatize[keyWordText]);
    //переменная, которая меняется в зависимости от того было ли найдено совпадение
    let result = false;
    //проходим по массиву французских заимствований
    for(let keyWordFrench in frenchLemmatize) {
      //аналогично приводим к единому виду лематизированное слово
      const wordsFrench = generateArr(keyWordFrench, frenchLemmatize[keyWordFrench]);
      //находим совпадения, если находим
      if (contains(wordsText, wordsFrench)) {
        //то меняем значение переменной на true
        result = true;
      }
    }
    //если совпадение было найдено, значит очередное слово в тексте было заимствованным из французского
    if (result) {
      //прибавляем единицу к французским словам
      frenchAmount++;
    }
  }

  //подсчитаваем количество самых популярных заимствованных слов
  //для этого проходим уже по целому не лематизированному тексу
  for(let i = 0; i < text.length; i++) {
    //проходим по списку французских слов
    for(let keyWordFrench in frenchLemmatize) {
      //аналогично прошлым действиям приводим в единый вид
      const wordsFrench = generateArr(keyWordFrench, frenchLemmatize[keyWordFrench]);
      //проверяем есть ли очередное слово в тексте в массиве заимствованных с французского
      if (wordsFrench.indexOf(text[i]) !== -1) {
        //если есть, то если поле под это слово уже было создано, просто прибавляем единицу к количеству
        if (frenchAmountTop[wordsFrench[0]]) {
          frenchAmountTop[wordsFrench[0]]++;
        } else {
          //иначе если слово попалось в первый раз то создаем поле с ключем, являющимся словом, а значение выставляем единицу
          frenchAmountTop[wordsFrench[0]] = 1;
        }
      }
    }
  }

  //преобразовать объекта в массив объектов для будущей сортировки и формирования топа
  let frenchAmountTopArr = Object
    .entries(frenchAmountTop)
    .map(([key, value]) => ({key, value}));

  //сортируем по убыванию количества использований
  frenchAmountTopArr
    .sort((itemA, itemB) => (itemB.value - itemA.value));

  //оставил только первые 50
  frenchAmountTopArr.length = 50
  const englishAmount = textAmount - frenchAmount;

  //вывод инфы
  console.log(text.length);
  console.log('top');
  console.log(frenchAmountTopArr);
  console.log('french', frenchAmount, `${(frenchAmount / textAmount * 100).toFixed(2)}%`);
  console.log('english', englishAmount, `${(englishAmount / textAmount * 100).toFixed(2)}%`);
  console.log('all', textAmount, '100%');
}

//функция, которая получает файлы по переданному маршруту
const getProcessedArrayOfWords = (pathToFile) => {
  const fullPath = path.join(__dirname, pathToFile);
  return new Promise((resolve, reject) => {
    textract.fromFileWithPath(fullPath, (err, text) => {
      if (err) reject('Error');
      else {
        //возвращаем сразу обработанный текст
        resolve(textProcessing(text));
      }
    });
  })
}

//функция, с которой всё начинается
const initCalc = async () => {
  //получаем содержимое файлов
  const [list, text] = await Promise.all([
    getProcessedArrayOfWords('/files/list.docx'),
    getProcessedArrayOfWords('/files/jerome.docx'),
  ])
  //передаем в основную функцию для вычислений массивы слов из списка и из текста
  calcPercentFrench(list, text);
}

//запуск процесса
initCalc();
