const { generateArr, contains, textProcessing } = require('./utils');

module.exports = {
  textProcessing,
  generateArr,
  contains,
}
