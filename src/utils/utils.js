//либа для стемминга
const snowball = require('node-snowball');

//вспомогательная функция чтобы приводить лемматизированные слова в единую форму (в форму массива слов)
// {word: false} = [word]
// {word : ['word', 'word2'] = [word, word2] }
const generateArr = (key, value) => {
  if (value) return value;
  return [key];
}

//сравнение слов из двух массивов на совпадение
const contains = (arr1, arr2) => {
  for (let i = 0; i < arr1.length; i++) {
    for (let j = 0; j < arr2.length; j++) {
      if (arr1[i] === arr2[j]) return true;
    }
  }
  return false;
}

//обработка полученного текста
const textProcessing = (text) => {
  const textAfterFilter = text
    .replace(/[^a-z A-Z]/gi, '') //убираем всё, кроме слов и пробелов
    .split(' ') //разбиваем строку на массив
    .filter(word => word !== ''); //на всякий случай убираем пустые элементы
  return snowball
    .stemword(textAfterFilter, 'english') // стемминг
    .map(word => word.toUpperCase()); //переводим в верхний регистр
}

module.exports = {
  textProcessing,
  generateArr,
  contains,
}
